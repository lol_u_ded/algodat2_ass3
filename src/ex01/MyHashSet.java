package ex01;

public interface MyHashSet {
	
	/**
	 * returns the number of stored keys (keys must be unique!).
	 */
    public int size(); 	

    /**
     * Inserts a key and returns true if it was successful. If there is already an entry with the 
     * same key or the hash table is full, the new key will not be inserted and false is returned.
     * 
     * @param key
     * 		The key which shall be stored in the hash table.
     * @param data
     * 		Any data object that shall be stored together with a key in the hash table.
     * @return
     * 		True if key could be inserted, or false if the key is already in the hash table.
     * @throws 
//     * 		an IllegalArgumentException if any of the input parameters is null.
     */ 
    public boolean insert(Integer key, Object data)  
      throws IllegalArgumentException;      
       
    /**
     *  Searches for a given key in the hash table.
     *  @param key
     *  	The key to be searched in the hash table.
     *  @return 
     *  	Returns true if the key is already stored, otherwise false.
     *  @throws
//     *  	an IllegalArgumentException if the key is null.
     */
    public boolean contains(Integer key) 
      throws IllegalArgumentException;  

    /**
     * Removes the key from the hash table and returns true on success, false otherwise.
     * @param key
     * 		The key to be removed from the hash table.
     * @return
     * 		True if the key was found and removed, false otherwise.
     * @throws
//     *  	an IllegalArgumentException if the key is null.
     */
    public boolean remove(Integer key)  
      throws IllegalArgumentException; 

    /**
     * Removes all stored elements from the hash table by setting all nodes to null.
     */
    public void clear(); 
    
    /**
     * (Required for testing only)
     * @return Return the hash table. 
     */
    public ChainingHashNode[] getHashTable();
    
    /**
     * (Required for testing only) Set a given hash table which shall be used. 
     * @param table
     * 		Given hash table which shall be used.
     * @param size
     * 		Number of already stored keys in the given table.
     */
    public void setHashTable(ChainingHashNode[] table, int size);
    
    /**d
     * Hash function that calculates a hash code for a given key using the modulo division.
     * @param key
     * 		Key for which a hash code shall be calculated according to the length of the hash table. 
     * @param hashTableLength
     * 		Length of the hash table.
     * @return
     * 		The calculated hash code for the given key.
     */
    public int getHashCode(Integer key, Integer hashTableLength);
    
}
