package ex01;

public class ChainingHashNode {
	Integer key; 
	Object data;
	ChainingHashNode next;

	ChainingHashNode(Integer key, Object data) {
		this.key = key;
		this.data = data;
		next = null;
	}
}
