package ex01;

public class ChainingHashSet implements MyHashSet {

	private final Integer capacity;
	private int size = 0;
	private ChainingHashNode[] hashTable;

	/**
	 * constructor initializes the hash table with the given capacity.
	 */
	public ChainingHashSet(int capacity) {
		hashTable = new ChainingHashNode[capacity];
		this.capacity = capacity;
	}

	@Override
	public int getHashCode(Integer key, Integer hashTableLength) {
		return key % hashTableLength;
	}
	
	@Override
	public ChainingHashNode[] getHashTable() {
		return this.hashTable;
	}
	
	@Override
	public void setHashTable(ChainingHashNode[] table, int size) {
		this.hashTable = table;
		this.size = size;
	}
	
	@Override
	public int size() {
		return size;
	}

	@Override
	public boolean insert(Integer key, Object data) throws IllegalArgumentException {
		if (key == null || data == null) {
			throw new IllegalArgumentException();
		}

		int hash_idx = getHashCode(key, hashTable.length);

		if (hashTable[hash_idx] == null) {
			hashTable[hash_idx] = new ChainingHashNode(key, data);
		} else {
			ChainingHashNode curr = hashTable[hash_idx];
			if (curr.key == key) {
				return false;
			}
			while (curr.next != null) {
				if (curr.next.key == key) {
					return false;
				} else {
					curr = curr.next;
				}
			}
			curr.next = new ChainingHashNode(key, data);
		}
		size++;
		return true;
	}

	@Override
	public boolean contains(Integer key) throws IllegalArgumentException {

		if (key == null) {
			throw new IllegalArgumentException();
		}
		int idx = getHashCode(key, hashTable.length);
		ChainingHashNode curr = hashTable[idx];
		while (curr != null) {
			if (curr.key == key) {
				return true;
			}
			curr = curr.next;
		}
		return false;
	}

	@Override
	public boolean remove(Integer key) throws IllegalArgumentException {
		if (key == null) {
			throw new IllegalArgumentException();
		}
		int idx = getHashCode(key, hashTable.length);
		ChainingHashNode curr = hashTable[idx];

		ChainingHashNode prev = null;
		while (curr != null) {
			if (curr.key == key) {
				if (prev == null) {
					if (curr.next == null) {
						hashTable[idx] = null;
					} else /*(prev != null)*/ {
						hashTable[idx] = curr.next;
					}
				} else /*(prev != null)*/ {
					if (curr.next == null) {
						prev.next = null;
					} else /*(prev != null)*/ {
						prev.next = curr.next;
					}
				}
				size--;
				return true;
			}
			prev = curr;
			curr = curr.next;
		}
		return false;

	}

	@Override
	public void clear() {
		for (int i = 0; i < hashTable.length; i++) {
			hashTable[i] = null;
		}
		size = 0;
	}
}
