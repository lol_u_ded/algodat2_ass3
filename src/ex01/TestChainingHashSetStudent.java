package ex01;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;


public class TestChainingHashSetStudent {

	private boolean insert(MyHashSet set, int key) {
		return set.insert(Integer.valueOf(key), String.valueOf(key));
	}

	private boolean contains(MyHashSet set, int key) {
		return set.contains(Integer.valueOf(key));
	}

	private boolean remove(MyHashSet set, int key) {
		return set.remove(Integer.valueOf(key));
	}

	@Test
	public void testSize() {
		ChainingHashSet set = new ChainingHashSet(11);
		assertEquals(0, set.size());

		assertTrue(insert(set, 5));
		assertEquals(1, set.size());
		assertTrue(insert(set, 6));
		assertEquals(2, set.size());
		assertTrue(insert(set, 7));
		assertEquals(3, set.size());
		assertTrue(insert(set, 8));
		assertEquals(4, set.size());
		assertTrue(insert(set, 9));
		assertEquals(5, set.size());
		assertTrue(insert(set, 10));
		assertEquals(6, set.size());
		assertTrue(insert(set, 11));
		assertEquals(7, set.size());
		assertTrue(insert(set, 12));
		assertEquals(8, set.size());
		assertTrue(insert(set, 13));
		assertEquals(9, set.size());
		assertTrue(insert(set, 14));
		assertEquals(10, set.size());
		assertTrue(insert(set, 15));
		assertEquals(11, set.size());
		assertTrue(insert(set, 25));
		assertEquals(12, set.size());
	}

	@Test
	public void testGetHashTable() {
		ChainingHashSet set = new ChainingHashSet(11);
		ChainingHashNode[] hashTable = set.getHashTable();
		for (ChainingHashNode n : hashTable) {
			assertTrue(n == null);
		}
	}

	@Test
	public void testInsertWithChaining() {
		ChainingHashSet set = new ChainingHashSet(11);
		assertEquals(0, set.size());
		assertTrue(insert(set, 5));
		assertEquals(1, set.size());
		assertFalse(insert(set, 5));
		assertEquals(1, set.size());
		assertTrue(insert(set, 6));
		assertEquals(2, set.size());
		assertTrue(insert(set, 11));
		assertEquals(3, set.size());
		assertTrue(insert(set, 12));
		assertEquals(4, set.size());
		assertTrue(insert(set, 13));
		assertEquals(5, set.size());
		assertTrue(insert(set, 17));
		assertEquals(6, set.size());


		// check content
		ChainingHashNode[] hashTable = set.getHashTable();
		// Index 0
		assertEquals(Integer.valueOf(11), hashTable[0].key);
		assertEquals(null, hashTable[0].next);
		// Index 1
		assertEquals(Integer.valueOf(12), hashTable[1].key);
		assertEquals(null, hashTable[1].next);
		// Index 2
		assertEquals(Integer.valueOf(13), hashTable[2].key);
		assertEquals(null, hashTable[2].next);
		// Index 3
		assertEquals(null, hashTable[3]);
		// Index 4
		assertEquals(null, hashTable[4]);
		// Index 5
		assertEquals(Integer.valueOf(5), hashTable[5].key);
		assertEquals(null, hashTable[5].next);
		// Index 6 (chaining)
		assertEquals(Integer.valueOf(6), hashTable[6].key);
		assertTrue(hashTable[6].next != null);
		assertEquals(Integer.valueOf(17), hashTable[6].next.key);
		assertEquals(null, hashTable[6].next.next);
		// Index 7
		assertEquals(null, hashTable[7]);
		// Index 8
		assertEquals(null, hashTable[8]);
		// Index 9
		assertEquals(null, hashTable[9]);
		// Index 10
		assertEquals(null, hashTable[10]);
	}


	@Test
	public void testClear() {
		ChainingHashSet set = new ChainingHashSet(11);
		ChainingHashNode[] hashTable = set.getHashTable();


		assertTrue(insert(set, 5));
		assertEquals(1, set.size());
		assertTrue(insert(set, 6));
		assertEquals(2, set.size());
		assertTrue(insert(set, 7));
		assertEquals(3, set.size());

		assertTrue(set.size() > 0);
		set.clear();
		assertEquals(0, set.size());

		for (int i = 0; i < hashTable.length; i++) {
			assertEquals(null, hashTable[i]);
		}
	}
}
