package ex01;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class test_Chain {
    private final int testTable1Size = 9;
    private final ChainingHashNode[] testTable1 = new ChainingHashNode[]{
            new ChainingHashNode(6, "6"),
            new ChainingHashNode(14, "14"),
            null,
            new ChainingHashNode(1, "1"),
            new ChainingHashNode(42, "42"),
            null,
            new ChainingHashNode(19, "19"),
            null,
            new ChainingHashNode(21, "21"),
            new ChainingHashNode(8, "8"),
            new ChainingHashNode(10, "10"),
            new ChainingHashNode(11, "11"),
            null};

    private boolean insert(ex01.MyHashSet set, int key) {
        return set.insert(Integer.valueOf(key), String.valueOf(key));
    }

    private boolean contains(ex01.MyHashSet set, int key) {
        return set.contains(Integer.valueOf(key));
    }

    private boolean remove(MyHashSet set, int key) {
        return set.remove(Integer.valueOf(key));
    }

    // Help method to print a hash table formated.
    private String printArray(ChainingHashNode[] arr) {

        StringBuilder sb = new StringBuilder();
        //	sb.append("\tHash table dump:\n");
        sb.append("\t" + String.format("%-10s", "Index:"));
        for (int i = 0; i < arr.length; i++) {
            sb.append(String.format("%04d|", i));
        }
        sb.append("\n\t");
        sb.append(String.format("%-10s", "Key:"));

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] != null)
                sb.append(String.format("%4d|", arr[i].key));
            else
                sb.append("    |");
        }
        sb.append("\n\t");
        sb.append(String.format("%-10s", "Flag:"));

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] != null)
                sb.append(String.format("%4s|", arr[i] == null ? "T" : "F"));
            else
                sb.append(String.format("%4s|", ""));
        }

        sb.append("\n");
        return sb.toString();
    }

    @Test
    public void testSizeWithInsertDuplicates() {
        ChainingHashSet set = new ChainingHashSet(17);

        Assert.assertTrue(insert(set, 5));
        assertEquals(1, set.size());
        assertFalse(insert(set, 5));
        assertEquals(1, set.size());
    }


    @Test
    public void testGetHashTable() {
        ChainingHashSet set = new ChainingHashSet(11);
        ChainingHashNode[] hashTable = set.getHashTable();
        for (ChainingHashNode n : hashTable) {
            assertTrue(n == null);
        }
    }

//    @Test
//    public void testContains() {
//        ChainingHashSet set = new ChainingHashSet(1);
//        set.setHashTable(testTable1, testTable1Size);
//
//        int[] testArrTrue = new int[]{6, 14, 1, 42, 19, 21, 8, 10};
//        int[] testArrFalse = new int[]{15, 111, 2};
//
//        for (int i : testArrTrue) {
//            Assert.assertTrue(".contains(" + i + ") returned FALSE for the following hash table:\n"
//                    + "\n" + printArray(set.getHashTable()) + "\t", contains(set, i));
//        }
//
//        for (int i : testArrFalse) {
//            assertFalse(".contains(" + i + ") returned TRUE for the following hash table:\n"
//                    + "\n" + printArray(set.getHashTable()) + "\t", contains(set, i));
//        }
//    }


}