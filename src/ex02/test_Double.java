package ex02;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class test_Double {
    private final int testTable1Size = 9;
    private final OpenHashNode[] testTable1 = new OpenHashNode[]{
            new OpenHashNode(6, "6"),
            new OpenHashNode(14, "14"),
            null,
            new OpenHashNode(1, "1"),
            new OpenHashNode(42, "42"),
            null,
            new OpenHashNode(19, "19"),
            null,
            new OpenHashNode(21, "21"),
            new OpenHashNode(8, "8"),
            new OpenHashNode(10, "10"),
            new OpenHashNode(11, "11"),
            null};

    private boolean insert(MyHashSet set, int key) {
        return set.insert(Integer.valueOf(key), String.valueOf(key));
    }

    private boolean contains(MyHashSet set, int key) {
        return set.contains(Integer.valueOf(key));
    }

    private boolean remove(MyHashSet set, int key) {
        return set.remove(Integer.valueOf(key));
    }

    // Help method to print a hash table formated.
    private String printArray(OpenHashNode[] arr) {

        StringBuilder sb = new StringBuilder();
        //	sb.append("\tHash table dump:\n");
        sb.append("\t" + String.format("%-10s", "Index:"));
        for (int i = 0; i < arr.length; i++) {
            sb.append(String.format("%04d|", i));
        }
        sb.append("\n\t");
        sb.append(String.format("%-10s", "Key:"));

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] != null)
                sb.append(String.format("%4d|", arr[i].key));
            else
                sb.append("    |");
        }
        sb.append("\n\t");
        sb.append(String.format("%-10s", "Flag:"));

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] != null)
                sb.append(String.format("%4s|", arr[i].removed ? "T" : "F"));
            else
                sb.append(String.format("%4s|", ""));
        }

        sb.append("\n");
        return sb.toString();
    }

    @Test
    public void testSizeWithInsertDuplicates() {
        DoubleHashSet set = new DoubleHashSet(17);

        Assert.assertTrue(insert(set, 5));
        assertEquals(1, set.size());
        assertFalse(insert(set, 5));
        assertEquals(1, set.size());
    }


    @Test
    public void testGetHashTable() {
        DoubleHashSet set = new DoubleHashSet(11);
        OpenHashNode[] hashTable = set.getHashTable();
        for (OpenHashNode n : hashTable) {
            assertTrue(n == null);
        }
    }

    @Test
    public void testContains() {
        DoubleHashSet set = new DoubleHashSet(1);
        set.setHashTable(testTable1, testTable1Size);

        int[] testArrTrue = new int[]{6, 14, 1, 42, 19, 21, 8, 10};
        int[] testArrFalse = new int[]{15, 111, 2};

        for (int i : testArrTrue) {
            Assert.assertTrue(".contains(" + i + ") returned FALSE for the following hash table:\n"
                    + "\n" + printArray(set.getHashTable()) + "\t", contains(set, i));
        }

        for (int i : testArrFalse) {
            assertFalse(".contains(" + i + ") returned TRUE for the following hash table:\n"
                    + "\n" + printArray(set.getHashTable()) + "\t", contains(set, i));
        }
    }


}