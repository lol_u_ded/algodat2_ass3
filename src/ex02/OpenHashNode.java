package ex02;

public class OpenHashNode {
  	Integer key; 
	Object data;
  	boolean removed = false;

  	OpenHashNode(Integer key, Object data) {
    	this.key = key;
    	this.data = data;
    	removed = false;
  	}
  	
}
