package ex02;

public class DoubleHashSet implements MyHashSet {

	private OpenHashNode[] hashTable;
	private int size = 0;
	private Integer cap;

	/**
	 * Initialisiert Hashtabelle mit der Kapazit�t capacity.
	 */
	public DoubleHashSet(int capacity) {
		this.cap = capacity;
		this.hashTable = new OpenHashNode[capacity];
		this.size = 0;
		//Arrays.fill(hashTable, new OpenHashNode(null, null));
	}

	@Override
	public final int getHashCode(Integer key, Integer hashTableLength) {
		return key % hashTableLength;
	}
	
	@Override
	public final int getHashCode2(Integer key, Integer hashTableLength) {
		int n = hashTableLength - 1;
		if (n == 0) {
			n = 1;
		}
		return 1 + (key % (n));
	}
	
	@Override
	public OpenHashNode[] getHashTable() {
		return this.hashTable;
	}
	
	@Override
	public void setHashTable(OpenHashNode[] table, int size) {
		this.hashTable = table;
		this.size = size;
	}
	
	@Override
	public int size() {
		return size;
	}

	@Override
	public boolean insert(Integer key, Object data) throws IllegalArgumentException {
		int cap = hashTable.length;
		if (key == null || data == null) {
			throw new IllegalArgumentException();
		}

		if (contains(key) || size == cap) {
			return false;
		}

		int hash_idx1 = getHashCode(key, cap);
		int hash_idx2 = getHashCode2(key, cap);
		int idx = getHashIdx(hash_idx1, hash_idx2);

		OpenHashNode curr = hashTable[idx];

		int counter = 0;
		while (counter < cap) {
			if (curr == null) {
				hashTable[idx] = new OpenHashNode(key, data);
				counter = cap;
			} else if (curr.key == null) {
				curr.key = key;
				curr.data = data;
				curr.removed = false;
				counter = cap;
			} else {
				idx = getHashIdx(idx, hash_idx2);
				curr = hashTable[idx];
				counter++;
			}
		}
		size++;
		return true;
	}

	@Override
	public boolean contains(Integer key) throws IllegalArgumentException {
		cap = hashTable.length;
		if (key == null) {
			throw new IllegalArgumentException();
		}

		int hash_idx1 = getHashCode(key, cap);
		int hash_idx2 = getHashCode2(key, cap);
		int idx = getHashIdx(hash_idx1, hash_idx2);

		OpenHashNode curr = hashTable[idx];

		int counter = 0;
		while (counter < cap) {
			if (curr == null) {
				idx = getHashIdx(idx, hash_idx2);
				curr = hashTable[idx];
				counter++;
			} else if (curr.key == key) {
				return true;
			} else {
				idx = getHashIdx(idx, hash_idx2);
				curr = hashTable[idx];
				counter++;
			}
		}
		return false;
	}

	@Override
	public boolean remove(Integer key) throws IllegalArgumentException {
		cap = hashTable.length;
		if (key == null) {
			throw new IllegalArgumentException();
		}

		int hash_idx1 = getHashCode(key, cap);
		int hash_idx2 = getHashCode2(key, cap);
		int idx = getHashIdx(hash_idx1, hash_idx2);

		OpenHashNode curr = hashTable[idx];

		int counter = 0;
		while (counter < cap) {
			if (curr == null) {
				return false;
			}
			if (curr.key == null && curr.removed == false) {
				return false;
			} else if (curr.key == key) {
				curr.key = null;
				curr.data = null;
				curr.removed = true;
				size--;
				return true;
			}
			idx = getHashIdx(idx, hash_idx2);
			curr = hashTable[idx];
			counter++;
		}
		return false;
	}

	@Override
	public void clear() {
		hashTable = new OpenHashNode[cap];
		size = 0;
		// Arrays.fill(hashTable, new OpenHashNode(null, null));
	}

	// Auxiliary methods
	private int getHashIdx(Integer hash1, Integer hash2) {
		return (hash1 + hash2) % cap;
	}
}
